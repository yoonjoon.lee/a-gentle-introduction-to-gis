.. _gentle_introduction_gis:

============================
A Gentle Introduction to GIS
============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:


   preamble
   introducing_gis
   vector_data
   vector_attribute_data
   data_capture
   raster_data
   topology
   coordinate_reference_systems
   map_production
   vector_spatial_analysis_buffers
   spatial_analysis_interpolation
   authors_license.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
