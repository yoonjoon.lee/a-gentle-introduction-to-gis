
.. _spatial_analysis:

*************************************************************
공간 분석 (보간) (Spatial Analysis (Interpolation)
*************************************************************

============= ===============================================================
학습목표        공간분석에서의 보간에 대한 이해
============= ===============================================================
Keywords      Point data, interpolation method, Inverse Distance Weighted,
              Triangulated Irregular Network
============= ===============================================================

개요
===============

**공간 분석(Spatial analysis)**\은 원시 데이터에서 새로운 정보와 의미를 추출하기 위해 공간 정보를 조작하는 과정이다. 통상적으로 공간 분석은 지리 정보 시스템(GIS)을 사용하여 수행한다. GIS는 통상적으로 특성 통계량(feature statistics) 계산과 데이터 보간법을 사용하여 지리 처리(geoprocessing) 활동을 수행하기 위한 공간 분석 도구를 제공한다. 수문학 사용자는 지형 분석과 수문 모델링(지상 및 지하 의 물 움직임을 모델링)의 중요성을 강조하고 있다. 야생 동물 관리자는 야생 동물 위치와 환경과의 관계 분석 기능에 관심이 있다. 사용자의 일에 따라 관심 사항이 다를 것이다.

공간 보간 (Spatial interpolation in detail)
======================================================

공간 보간은 알려진 값이 있는 점들을 사용하여 알려지지 않은 점의 값을 추정하는 방법이다. 예를 들어, 해당 국가에 대한 강수량 지도를 작성하기 위해, 전체 지역에 걸쳐 기상 관측소를 설치할 수는 없다. 공간 보간법은 인근 기상대에서 알려진 온도 값을 사용하여 기록된 데이터가 없는 위치의 온도를 추정할 수 있다 (:numref:`figure_temperature_map` 참조). 이러한 유형의 보간 면을 **통계 면(statistical surface)**\이라 한다. 표고 데이터, 강수량, 적설량, 물 테이블, 인구 밀도 등은 보간법을 사용하여 계산할 수 있는 데이터 유형들이다.

.. _figure_temperature_map:

.. figure:: img/temperature_map.png
   :align: center
   :width: 30em

   남아프리카 기상청에서 보간한 온도 지도.

높은 비용과 제한된 리소스로 인해, 통상적으로 제한된 수의 선택된 위치에서만 데이터 수집을 수행한다. GIS에서 이러한 점들을 바탕으로 공간 보간법을 적용하여 모든 래스터 셀에 대한 추정치를 구하여 래스터 면을 생성할 수 있다.

예를 들어 GPS 장치로 측정한 표고점에서 디지털 표고 값을 값는 연속 지도를 생성하려면 샘플이 아니거나 측정이 이루어지지 않은 위치에서 값을 최적으로 추정하는 적절한 보간법을 사용하여야 한다. 이후 보간 분석의 결과를 전체 영역을 포괄하는 분석과 모델링에 사용할 수 있다.

많은 보간법이 알려져 있지만, 여기에서는 널리 사용되는 두 가지 보간법인 **역거리 가중치(IDW: Inverse Distance Weighting)**\와 **삼각 불규칙 네트워크(TIN: Triangulated Irregular Networks)**\를 설명한다. 더 많은 보간법에 대하여 알고 싶다면 이 페이지 끝의 '참고 문헌' 섹션을 참고하십시오.

역거리 가중치 (Inverse Distance Weighted (IDW))
====================================================

IDW 보간법에서는 보간 값 계산을 위하여 샘플 포인트에서 추정하려는 포인트까지 거리에 따라 상대적인 영향력이 감소하는 가중치를 부여한다 (:numref:`figure_idw_interpolation` 참조).

.. _figure_idw_interpolation:

.. figure:: img/idw_interpolation.png
   :align: center
   :width: 30em

   왼쪽은 가중 표본 점 거리를 기준으로 한 IDW 보간법을 보이며. 오른쪽은 표고 벡터 점들로부터 IDW 보간법을 적용한 면이다. (이미지 출처: 미타스, L, 미타소바, H. (1999))

새로운 점에서 거리가 증가함에 따라 가중치 영향 감소를 제어하는 가중치 계수 도입을 통해 표본 점에 가중치를 할당한다. 보간 과정 중에 보간 점으로부터 가중치 계수가 클수록 표본 점들의 효과는 커지며 멀수록 효과는 더 적어진다. 계수가 증가하면 보간 점 값은 가장 근접한 관측점 값에 가까워 진다.

IDW 보간법에는 몇 가지 단점이 있다. 표본 데이터 점의 분포가 불균일할 경우 보간 결과의 품질이 저하될 수 있다. 또한 보간 면의 최대 및 최소값은 항상 표본 데이터 점들에서만 발생할 수 있다. 이로 인해 종종 :numref:`figure_idw_interpolation`\에 표시된 것처럼 표본 데이터 점들 주위의 피크가 작아진다.

GIS에서 보간 결과는 보통 2차원 래스터 레이어로 표시된다. :numref:`figure_idw_result`\에서 GPS 장치를 사용하여 현장에서 수집한 표고 샘플 포인트를 기준으로 얻은 일반적인 IDW 보간 결과를 볼 수 있다.

.. _figure_idw_result:

.. figure:: img/idw_result.png
   :align: center
   :width: 30em

   (검은색 십자가로 표시된) 불규칙하게 수집된 표고 샘플 포인트에서 얻은 IDW 보간 결과.

삼각 불규칙 네트워크 (TIN: Triangulated Irregular Network)
=========================================================================

TIN 보간법은 GIS에서 많이 사용되는 도구이다. TIN 알고리즘을 통상적으로 **델라우나이 삼각 측량법(Delaunay triangulation)**\이라 한다. 가장 가까운 이웃 점들과 삼각형으로 형성된 표면을 만들려고 한다. 이를 위해, 선택된 샘플 포인트를 중심으로 원을 생성하고, 겹치지 않는 가능한 한 작은 삼각형 네트워크에 원의 교차점을 연결한다 (:numref:`figure_tin_interpolation` 참조).

.. _figure_tin_interpolation:

.. figure:: img/tin_interpolation.png
   :align: center
   :width: 30em

   왼쪽은 빨간색 표본 데이터를 중심으로 원을 그리는 삼각 측량이며, 오른쪽은 표고 벡터 점으로부터 생성된 결과로 보간된 TIN 면이다. (이미지 출처: 미타스, L, 미타소바, H. (1999))

TIN 보간의 주요 단점은 표면이 매끄럽지 않고 들쭉날쭉한 외관을 가질 수 있다는 것이다. 이는 삼각형 가장자리와 표본 데이터 점의 불연속성 때문에 발생한다. 또한, 삼각 측정은 일반적으로 표본 데이터 점이 수집된 영역을 벗어나는 보외(extrapolation)에는 적합하지 않다 (:numref:`figure_tin_result` 참조).

.. _figure_tin_result:

.. figure:: img/tin_result.png
   :align: center
   :width: 30em

   불규칙하게 수집된 강우 샘플 (파란색) 포인트들로 부터의 델라우나이 TIN 보간 결과

유의 사항
=======================================

모든 상황에 적용할 수 있는 단일 보간법이 없다는 점을 명심해야 한다. 일부는 다른 것들보다 더 정확하고 유용하지만 계산하는 데 시간이 더 걸린다. 그들은 모두 장점과 단점을 가지고 있다. 실제로 특정 보간법의 선택은 표본 데이터, 생성될 면의 유형과 추정 오차의 허용 범위에 따라 달라져야 한다. 일반적으로 3단계 절차를 권장한다.

#. 표본 데이터를 평가한다. 이를 통해 데이터가 해당 영역에 어떻게 분포되어 있는지 알 수 있으며, 사용할 보간 방법에 대한 힌트를 얻을 수 있다.
#. 표본 데이터와 연구 목표 모두에 가장 적합한 보간법을 적용한다. 문제가 있을 경우 다른 여러 가지 방법을 시도한다.
#. 결과를 비교하여 최상의 결과와 가장 적합한 방법을 찾는다. 처음에는 시간이 많이 걸리는 프로세스처럼 보일 수 있으나 다양한 보간 방법에 대한 경험과 지식을 습득하면 가장 적합한 면을 생성하는 데 필요한 시간이 크게 줄어들 것이다.

다른 보간법 (other interpolation methods)
=====================================================

이 페이지에서 IDW와 TIN 보간법에 중점을 두었지만, GIS는 RST(Regularized Splines with Tension), Kriging 또는 Trend Surface interpolation과 같은 다양한 공간 보간법을 제공한다 (아래 '참고 문헌' 섹션의 웹 링크를 참조).

학습 내용
===============

이 장에서 설명한 내용을 다음과 같이 간단히 정리해 볼 수 있다.

* **보간(Interpolation)**\은 알려진 값이 있는 벡터 점으로 부터 값을 알 수 없는 위치의 값을 추정하여 전체 영역의 래스터 면을 생성한다.
* 보간 결과는 일반적으로 **래스터** 레이어이다.
* 모르는 위치에 대한 값을 최적으로 추정하려면 **적합한 보간법**\을 찾는 것이 중요하다.
* **IDW 보간**\은 표본 점에 가중치를 부여하여 추정하려는 새 점과의 거리에 따라 한 점이 다른 점에 미치는 영향을 감소하도록 한다.
* **TIN 보간**\은 표본 점을 사용하여 가장 인접한 지점 정보를 기반으로 삼각형으로 형성된 표면을 생성한다.

참고 문헌
============

**Books**:

* Chang, Kang-Tsung (2006). Introduction to Geographic Information Systems. 3rd Edition. McGraw Hill. ISBN: 0070658986
* DeMers, Michael N. (2005): Fundamentals of Geographic Information Systems. 3rd Edition. Wiley. ISBN: 9814126195
* Mitas, L., Mitasova, H. (1999). Spatial Interpolation. In: P.Longley, M.F. Goodchild, D.J. Maguire, D.W.Rhind (Eds.), Geographical Information Systems: Principles, Techniques, Management and Applications, Wiley.

**Websites**:

* https://en.wikipedia.org/wiki/Interpolation
* https://en.wikipedia.org/wiki/Delaunay_triangulation

QGIS User Guide에서 QGIS에서 제공하는 인터폴레이션 도구에 관한 보다 자세한 정보를 찾아 볼 수 있다.

다음은?
=======

이 페이지가 마지막 페이지이다. QGIS에 대해 알아보고 QGIS 매뉴얼을 사용하여 GIS 소프트웨어로 할 수 있는 다른 모든 기능들에 대하여 알아 보길 바랍니다!

.. |gentleLogo| image:: img/gentlelogo.png
   :width: 3em
