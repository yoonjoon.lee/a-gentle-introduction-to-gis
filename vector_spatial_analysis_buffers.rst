
*********************************************************
공간 벡터 분석 (버퍼) (Vector Spatial Analysis (Buffers))
*********************************************************

============= ===============================================================
학습목표        벡터 공간 분석에서 버퍼링 사용에 대한 이해
============= ===============================================================
Keywords      Vector, buffer zone, spatial analysis, buffer distance,
              dissolve boundary, outward and inward buffer, multiple buffer
============= ===============================================================

개요
===============

공간 정보를 사용하여 **공간 분석(Spatial analysis)**\으로 GIS 데이터에서 새롭고 추가적인 의미를 추출할 수 있다. 일반적으로 GIS 애플리케이션을 사용하여 공간 분석을 수행한다. GIS 응용 프로그램에는 일반적으로 특성 통계(feature ststistics)(예: 폴리라인을 구성하는 정점 갯수?) 또는 특성 버퍼링(feature buffering)과 같은 지오프로세싱을 위한 공간 분석 도구가 있다. 주제 영역에 따라 사용하는 공간 분석 유형은 다를 수 있다. 물 관리와 연구(수문학)를 하는 사람들은 지형 분석과 교차 이동하는 물 모델링에 관심을 갖고 있을 것이다. 야생 동물 관리에서 사용자는 야생 동물 지점 위치와 환경과의 관계 분석 기능에 관심을 갖는다. 이 페이지에서는 벡터 데이터로 수행할 수 있는 유용한 공간 분석의 예로 버퍼링에 대해 설명한다.

버퍼링 (Buffering in detail)
==================================

통상적으로 **버퍼링**\은 두 종류의 영역을 생성한다. 하나는 실제 특성으로 부터 명시된 거리 **내부** 영역이며, 다른 하나는 **외부** 영역이다. 명시된 거리 내에 있는 영역을 **버퍼 영역(buffer zone)**\이라 한다.

**버퍼 영역**\은 실제 특성들을 분리하기 위하여 사용하는 영역이다. 버퍼 영역은 환경보호를 위하거나, 산업 재해, 자연 재해 또는 폭력으로부터 주거와 상업 지구를 보호하기 위하여 설치하는 경우가 많다. 버펴 영역의 흔한 형태로는 주거지역과 상업지역 사이의 그린벨트, 국가간의 국경지대 (:numref:`figure_buffer_zone` 참조), 공항 주변의 소음 보호구역 또는 하천변 오염 방지구역 등이 있다.

.. _figure_buffer_zone:

.. figure:: img/buffer_zone.png
   :align: center
   :width: 30em

   미국과 멕시코 사이의 국경은 버퍼 영역으로 분리되어 있다 (SGT Jim Greenhill이 찍은 사진, 2006).

GIS 응용 프로그램에서 **버퍼 영역**\은 다른 폴리곤, 선 또는 점 특성을 둘러싸는 **벡터 폴리곤 (vector polygons)**\으로 표시된다 (:numref:`figure_point_buffer`, :numref:`figure_line_buffer`, :numref:`figure_polygon_buffer` 참조).

.. _figure_point_buffer:

.. figure:: img/point_buffer.png
   :align: center
   :width: 30em

   벡터 점들 주위의 버퍼 영역.

.. _figure_line_buffer:

.. figure:: img/line_buffer.png
   :align: center
   :width: 30em

   벡터 폴리라인들 주위의 버퍼 영역.

.. _figure_polygon_buffer:

.. figure:: img/polygon_buffer.png
   :align: center
   :width: 30em

   벡터 폴리곤들 주위의 버퍼 영역.

버퍼링의 변형 (Variations in buffering)
=============================================

버퍼링에는 몇 가지 변형이 있다. **버퍼 거리(buffer distance)** 또는 버퍼 크기는 각 특성의 벡터 계층 속성 표에 명시된 숫자 값에 따라 달라질 수 있다. 수치 값은 데이터와 함께 사용되는 좌표 참조 시스템(CRS)에 따라 지도에서 사용하는 단위로 정의해야 한다. 예를 들어, 강둑을 따라 있는 버퍼 영역 너비는 인접한 토지 이용률에 따라 달라질 수 있다. 집중 재배의 경우 버퍼 거리는 유기 농업의 경우보다 클 수 있다 (:numref:`figure_variable_buffer` 및 table_buffer_attributes_ 참조).

.. _figure_variable_buffer:

.. figure:: img/variable_buffer.png
   :align: center
   :width: 30em

   버퍼 거리가 다른 버퍼링 강.

.. _table_buffer_attributes:

+--------------+---------------------------------+--------------------------+
| River        | Adjacent land use               | Buffer distance (meters) |
+==============+=================================+==========================+
| Breede River | Intensive vegetable cultivation | 100                      |
+--------------+---------------------------------+--------------------------+
| Komati       | Intensive cotton cultivation    | 150                      |
+--------------+---------------------------------+--------------------------+
| Oranje       | Organic farming                 | 50                       |
+--------------+---------------------------------+--------------------------+
| Telle river  | Organic farming                 | 50                       |
+--------------+---------------------------------+--------------------------+

표 버퍼 속성 : 인접한 토지 사용 정보에 따라 다른 하천의 버퍼 거리 속성 테이블.

하천이나 도로와 같은 폴리라인 특성 주위의 버퍼는 선의 양쪽 모두 있을 필요는 없다. 라인 특성의 왼쪽 또는 오른쪽에 있을 수 있다. 이 경우 디지타이징 동안 시작점에서 선의 끝점까지의 방향에 따라 왼쪽 또는 오른쪽으로 결정된다.

다중 버퍼 영역 (Multiple buffer zones)
-----------------------------------------------------

한 특성에 두 개 이상의 버퍼 영역이 있을 수도 있다. 원자력 발전소는 10, 15, 25 및 30 km의 거리에 따라 완충될 수 있으므로 대피 계획의 일부로 발전소 주변에 여러 링을 구성할 수 있다 (:numref:`figure_multiple_buffers` 참조).

.. _figure_multiple_buffers:

.. figure:: img/multiple_buffers.png
   :align: center
   :width: 30em

   10, 15, 25 및 30km 거리의 지점 특성 버퍼링.

온전하거나 용해된 경계의 버퍼링 (Buffering with intact or dissolved boundaries)
--------------------------------------------------------------------------------------

버퍼 영역에는 버퍼 영역 사이에 겹치는 영역이 없도록 용해된 경계선이 종종 나타난다. 그러나 경우에 따라 버퍼 영역의 경계를 그대로 유지하는 것이 유용할 수 있어 각 버퍼 영역을 별도의 폴리곤으로 하면 겹치는 영역을 식별할 수 있다 (:numref:`figure_buffer_dissolve` 참조).

.. _figure_buffer_dissolve:

.. figure:: img/buffer_dissolve.png
   :align: center
   :width: 30em

   중첩이 있는 영역을 보이기 위한 버퍼 영역(왼쪽)과 온전한 경계(오른쪽)가 있는 버퍼 영역.

외부 와 내부 버퍼링 (Buffering outward and inward)
-------------------------------------------------------------------

폴리곤 특성 주위의 버퍼 영역은 일반적으로 폴리곤 경계에서 바깥쪽으로 확장되지만 폴리곤 경계에서 안쪽으로 버퍼 영역을 만들 수도 있다. 예를 들어, 관광부는 로벤 섬을 일주하는 새로운 도로 건설을 계획하고 있으며 환경법은 도로가 해안선에서 안쪽으로 적어도 200미터 떨어져 있어야 한다고 규정하고 있다. 해안선 안쪽 200m 선을 찾기 위해 내부 버퍼를 사용할 수 있으며, 그 선을 넘지 않도록 도로를 설계할 수 있다.

유의 사항
=================

대부분 GIS 응용 프로그램은 분석 도구로 버퍼 생성을 제공하지만 버퍼 생성 옵션은 다를 수 있다. 예를 들어, 일부 GIS 응용프로그램은 라인 특성의 왼쪽 또는 오른쪽에 버퍼링하거나 버퍼 영역의 경계를 용해하거나 폴리곤 경계 안쪽으로 버퍼링할 수 없다.

버퍼 거리는 항상 정수 또는 실수로 설정해야 한다. 이 값은 벡터 계층의 좌표 참조 시스템(CRS)에 따라 지도 단위(미터, 피트, 십진도)를 사용한다.

다른 공간 분석 도구
===========================

버퍼링은 중요하고 자주 사용하는 공간 분석 도구이지만 GIS에서 사용되고, 사용자가 탐색할 수 있는 다른 도구들이 많이 있다.

**공간 오버레이(Spatial overlay)**\는 동일한 영역의 전체 또는 일부를 공유하는 두 폴리곤 특성 간의 관계를 식별할 수 있는 프로세스이다. 출력 벡터 계층은 입력 특성 정보의 조합이다 (:numref:`figure_overlay_operations` 참조).

.. _figure_overlay_operations:

.. figure:: img/overlay_operations.png
   :align: center
   :width: 30em

   두 개의 입력 벡터 레이어(a_input = 직사각형, b_input = 원)가 있는 공간 오버레이. 결과 벡터 레이어는 녹색으로 표시되었다.

전형적인 공간 오버레이 예는 다음과 같다.

* **교집합(Intersection)**\: 두 계층이 겹치는(intersect) 모든 영역을 출력한다 .
* **합집합(Union)**\: 두 입력 계층을 합친 모든 영역을 출력한다.
* **대칭 차집합(Symmetrical difference)**\: 두 계층이 겹치는(intersect) 영역을 제외한 입력 계층의 모든 영역을 출력한다.
* **차집합(Difference)**\: 두 번째 입력 계층과 겹치지(intersect) 않는 첫 번째 입력 계층의 모든 영역을 출력한다.

학습 내용
===============

이 장에서 설명한 내용을 다음과 같이 간단히 정리해 볼 수 있다.

* **버퍼 영역(Buffer zones)**\은 실제 특성 주위의 영역을 기술한다.
* 버퍼 영역은 항상 **벡터 다각형(vector polygons)**\이다.
* 한 특성은 **다중** 버퍼 영역을 갖을 수 있다.
* **버퍼 거리(buffer distance)**\는 퍼퍼 영역 크기를 결정한다.
* 버퍼 거리는 **정수** 또는 **실수**\형 이어야 한다.
* 버퍼 거리는 벡터 레이어내에서 각 특성마다 다를 수 있다.
* 다각형 경계로부터 **내부(inward)**\와 **외부(outward)**\로 다각형을 버퍼링할 수 있다.
* 온전하거나 용해된 경계로 버퍼 영역을 생성한다.
* 버퍼링 외에도 GIS는 일반적으로 공간 작업을 해결하기 위한 다양한 벡터 분석 도구를 제공한다.


더 읽을 거리
============

**Books**:

* Galati, Stephen R. (2006). Geographic Information Systems Demystified. Artech
  House Inc. ISBN: 158053533X
* Chang, Kang-Tsung (2006). Introduction to Geographic Information Systems. 3rd
  Edition. McGraw Hill. ISBN: 0070658986
* DeMers, Michael N. (2005). Fundamentals of Geographic Information Systems. 3rd
  Edition. Wiley. ISBN: 9814126195

QGIS에서 제공하는 벡터 데이터 분석에 관한 보다 자세한 정보를 QGIS User Guide에서 찾아 볼 수 있다.

다음은?
=======

다음 장에서는 래스터 데이터를 사용하여 수핼할 수 있는 공간 분석의 한 예로 **보간법 (interpolation)**\에 대해 자세히 살펴본다.

.. |gentleLogo| image:: img/gentlelogo.png
  :width: 3em
