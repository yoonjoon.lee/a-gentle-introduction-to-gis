
*************************************************************
저자, 기여자 및 저작권
*************************************************************

이 소개 문서 작성을 위하여 Tim Sutton, Otto Dassau, Marcelle Sutton, Lerato Nsibander과 Sibongile Mthombeni이 수고 하였다. (자세한 내용은 `About the authors & contributors <https://docs.qgis.org/3.16/en/docs/gentle_gis_introduction/authors_and_contributors.html>`_\를 참고하십시요)

또한 이 문서에 대한 저작권은 전적으로 `GNU Free Documentation License <https://docs.qgis.org/3.16/en/docs/gentle_gis_introduction/gnu_free_documentation_license.html>`_\를 따르고 있습니다.  
