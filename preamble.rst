.. _A-Gentle-Introduction-To-GIS-reference:

********
서언
********

A Gentle Introduction to GIS

번역에 부쳐
======================

이 웹 페이지는 오픈 소스 지리정보시스템(GIS, Geographic Information System) 소프트웨어를 제공하는 웹 사이트 https://www.qgis.org 에서 GIS에 대한 간략한 소개인 "A Gentle Introduction to GIS (https://docs.qgis.org/3.16/en/docs/gentle_gis_introduction/index)"를 번역한 것입니다. GIS를 입문하는 사람들에게 GIS를 간략히 소개하여 원활한 QGIS 사용을 돕기 위함입니다.

2021년 3월, 대전에서 이 윤 준
